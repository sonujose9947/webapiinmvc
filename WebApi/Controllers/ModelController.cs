﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Data;
using System.Data.SqlClient;
using WebApi.Models;
using System.Configuration;
using System.Web.UI.WebControls;
namespace WebApi.Controllers
{
    public class ModelController : ApiController
    {
        SqlConnection sqlCon = null;
        string con = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        SqlDataReader sdr;
        public IHttpActionResult getModel()
        {
            List<TblModel> cls = new List<TblModel>();

            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteModel @ModelId, @ModelName,@ClassId, @TypeId, @BrandId, @Option";

                cmd.Parameters.Add("@ModelId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ModelName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Select";
                sqlCon.Open();
                sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    cls.Add(new TblModel()
                    {
                        MODEL_ID = Convert.ToInt32(sdr.GetValue(0)),
                        MODEL_NAME = sdr.GetValue(1).ToString(),
                        CLASS_ID = Convert.ToInt32(sdr.GetValue(2)),
                        CLASS_NAME = sdr.GetValue(3).ToString(),
                        TYPE_ID = Convert.ToInt32(sdr.GetValue(4)),
                        TYPE_NAME = sdr.GetValue(5).ToString(),
                        BRAND_ID = Convert.ToInt32(sdr.GetValue(6)),
                        BRAND_NAME = sdr.GetValue(7).ToString(),

                    });
                }
                sqlCon.Close();
                return Ok(cls);
            }
        }
        public IHttpActionResult createModel(TblModel dbModel)
        {
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteModel @ModelId, @ModelName,@ClassId, @TypeId, @BrandId, @Option";

                cmd.Parameters.Add("@ModelId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = dbModel.BRAND_ID;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = dbModel.TYPE_ID;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = dbModel.CLASS_ID;
                cmd.Parameters.Add("@ModelName", SqlDbType.VarChar, 50).Value = dbModel.MODEL_NAME;
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Create";

                cmd.ExecuteNonQuery();
                sqlCon.Close();
                return Ok();
            }
        }
        public IHttpActionResult getModelById(int id)
        {
            TblModel cls = new TblModel();
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteModel @ModelId, @ModelName,@ClassId, @TypeId, @BrandId, @Option";

                cmd.Parameters.Add("@ModelId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ModelName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "GetById";
                sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    cls.MODEL_ID = Convert.ToInt32(sdr.GetValue(0));
                    cls.MODEL_NAME = sdr.GetValue(1).ToString();
                    cls.CLASS_ID = Convert.ToInt32(sdr.GetValue(2));
                    cls.TYPE_ID = Convert.ToInt32(sdr.GetValue(3));
                    cls.BRAND_ID = Convert.ToInt32(sdr.GetValue(4));
                }
                sqlCon.Close();
                return Ok(cls);
            }
        }
        [HttpGet]
        [Route("api/Model/getModelByBrandId/{id?}")]
        public IHttpActionResult getModelByBrandId(int id)
        {
            List<TblModel> cls = new List<TblModel>();

            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteModel @ModelId, @ModelName,@ClassId, @TypeId, @BrandId, @Option";

                cmd.Parameters.Add("@ModelId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ModelName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "GetByBrandId";
                sqlCon.Open();
                sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    cls.Add(new TblModel()
                    {
                        MODEL_ID = Convert.ToInt32(sdr.GetValue(0)),
                    MODEL_NAME = sdr.GetValue(1).ToString(),
                    CLASS_ID = Convert.ToInt32(sdr.GetValue(2)),
                    TYPE_ID = Convert.ToInt32(sdr.GetValue(3)),
                    BRAND_ID = Convert.ToInt32(sdr.GetValue(4))
                });
                }
                sqlCon.Close();
                return Ok(cls);
            }
        }
        [HttpGet]
        [Route("api/Model/checkifexists/{id?}")]
        public IHttpActionResult checkifexists(int id)
        {
            
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteModel @ModelId, @ModelName,@ClassId, @TypeId, @BrandId, @Option";

                cmd.Parameters.Add("@ModelId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ModelName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "IfExists";
                sqlCon.Open();
                var a = cmd.ExecuteScalar();

                sqlCon.Close();
                return Ok(a);
            }
        }
        [HttpPut]
        public IHttpActionResult editModel(int id, TblModel dbModel)
        {
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteModel @ModelId, @ModelName,@ClassId, @TypeId, @BrandId, @Option";

                cmd.Parameters.Add("@ModelId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = dbModel.BRAND_ID;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = dbModel.TYPE_ID;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = dbModel.CLASS_ID;
                cmd.Parameters.Add("@ModelName", SqlDbType.VarChar, 50).Value = dbModel.MODEL_NAME;
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Edit";

                cmd.ExecuteNonQuery();
                sqlCon.Close();
                return Ok();
            }
        }
        [HttpDelete]
        public IHttpActionResult deleteModel(int id)
        {
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteModel @ModelId, @ModelName,@ClassId, @TypeId, @BrandId, @Option";

                cmd.Parameters.Add("@ModelId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ModelName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Delete";

                cmd.ExecuteNonQuery();
                sqlCon.Close();
                return Ok();
            }
        }
    }
}
