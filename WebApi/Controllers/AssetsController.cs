﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Data;
using System.Data.SqlClient;
using WebApi.Models;
using System.Configuration;
using System.Web.UI.WebControls;
namespace WebApi.Controllers
{
    public class AssetsController : ApiController
    {
        SqlConnection sqlCon = null;
        string con = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        SqlDataReader sdr;
        public IHttpActionResult getAsset()
        {
            List<TblAssets> cls = new List<TblAssets>();

            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteAssets @AssetId, @AssetName ,@AssetPrice,@InstalledDate," +
                    "@ClassId, @TypeId ,@BrandId,@ModelId, @Option";

                cmd.Parameters.Add("@AssetId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@AssetName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@AssetPrice", SqlDbType.Decimal).Value = 0;
                cmd.Parameters.Add("@InstalledDate", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@ModelId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Select";
                sqlCon.Open();
                sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    cls.Add(new TblAssets()
                    {
                        ASSET_ID = Convert.ToInt32(sdr.GetValue(0)),
                        ASSET_NAME = sdr.GetValue(1).ToString(),
                        ASSET_PRICE = (decimal)sdr.GetValue(2),
                        INSTALLED_DATE = (DateTime)sdr.GetValue(3),
                        CLASS_ID = Convert.ToInt32(sdr.GetValue(4)),
                        CLASS_NAME = sdr.GetValue(5).ToString(),
                        TYPE_ID = Convert.ToInt32(sdr.GetValue(6)),
                        TYPE_NAME = sdr.GetValue(7).ToString(),
                        BRAND_ID = Convert.ToInt32(sdr.GetValue(8)),
                        BRAND_NAME = sdr.GetValue(9).ToString(),
                        MODEL_ID = Convert.ToInt32(sdr.GetValue(10)),
                        MODEL_NAME = sdr.GetValue(11).ToString(),
                        
                    });
                }
                sqlCon.Close();
                return Ok(cls);
            }
        }
        public IHttpActionResult createAsset(TblAssets dbAsset)
        {
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteAssets @AssetId, @AssetName ,@AssetPrice,@InstalledDate," +
                    "@ClassId, @TypeId ,@BrandId,@ModelId, @Option";

                cmd.Parameters.Add("@AssetId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@AssetName", SqlDbType.VarChar, 50).Value = dbAsset.ASSET_NAME;
                cmd.Parameters.Add("@AssetPrice", SqlDbType.Decimal).Value = dbAsset.ASSET_PRICE;
                cmd.Parameters.Add("@InstalledDate", SqlDbType.DateTime).Value = dbAsset.INSTALLED_DATE;
                cmd.Parameters.Add("@ModelId", SqlDbType.Int).Value = dbAsset.MODEL_ID;
                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = dbAsset.BRAND_ID;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = dbAsset.TYPE_ID;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = dbAsset.CLASS_ID;
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Create";

                cmd.ExecuteNonQuery();
                sqlCon.Close();
                return Ok();
            }
        }
        public IHttpActionResult getAssetById(int id)
        {
            TblAssets cls = new TblAssets();
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteAssets @AssetId, @AssetName ,@AssetPrice,@InstalledDate," +
                    "@ClassId, @TypeId ,@BrandId,@ModelId, @Option";

                cmd.Parameters.Add("@AssetId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@AssetName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@AssetPrice", SqlDbType.Decimal).Value = 0;
                cmd.Parameters.Add("@InstalledDate", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@ModelId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "GetById";
                sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    cls.ASSET_ID = Convert.ToInt32(sdr.GetValue(0));
                    cls.ASSET_NAME = sdr.GetValue(1).ToString();
                    cls.ASSET_PRICE = (decimal)sdr.GetValue(2);
                    cls.INSTALLED_DATE = (DateTime)sdr.GetValue(3);
                    cls.CLASS_ID = Convert.ToInt32(sdr.GetValue(4));
                    cls.TYPE_ID = Convert.ToInt32(sdr.GetValue(5));
                    cls.BRAND_ID = Convert.ToInt32(sdr.GetValue(6));
                    cls.MODEL_ID = Convert.ToInt32(sdr.GetValue(7));
                }
                sqlCon.Close();
                return Ok(cls);
            }
        }
        [HttpPut]
        public IHttpActionResult editModel(int id, TblAssets dbAsset)
        {
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteAssets @AssetId, @AssetName ,@AssetPrice,@InstalledDate," +
                    "@ClassId, @TypeId ,@BrandId,@ModelId, @Option";

                cmd.Parameters.Add("@AssetId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@AssetName", SqlDbType.VarChar, 50).Value = dbAsset.ASSET_NAME;
                cmd.Parameters.Add("@AssetPrice", SqlDbType.Decimal).Value = dbAsset.ASSET_PRICE;
                cmd.Parameters.Add("@InstalledDate", SqlDbType.DateTime).Value = dbAsset.INSTALLED_DATE;
                cmd.Parameters.Add("@ModelId", SqlDbType.Int).Value = dbAsset.MODEL_ID;
                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = dbAsset.BRAND_ID;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = dbAsset.TYPE_ID;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = dbAsset.CLASS_ID;
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Edit";

                cmd.ExecuteNonQuery();
                sqlCon.Close();
                return Ok();
            }
        }
        [HttpDelete]
        public IHttpActionResult deleteAsset(int id)
        {
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteAssets @AssetId, @AssetName ,@AssetPrice,@InstalledDate," +
                    "@ClassId, @TypeId ,@BrandId,@ModelId, @Option";

                cmd.Parameters.Add("@AssetId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@AssetName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@AssetPrice", SqlDbType.Decimal).Value = 0;
                cmd.Parameters.Add("@InstalledDate", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@ModelId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Delete";

                cmd.ExecuteNonQuery();
                sqlCon.Close();
                return Ok();
            }
        }
    }
}
