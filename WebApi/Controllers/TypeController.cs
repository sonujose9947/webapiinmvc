﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using WebApi.Models;
using System.Configuration;
using System.Web.UI.WebControls;

namespace WebApi.Controllers
{
    public class TypeController : ApiController
    {
        SqlConnection sqlCon = null;
        string con = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        SqlDataReader sdr;
        public IHttpActionResult getType()
        {
            List<TblType> cls = new List<TblType>();

            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteType @TypeId, @TypeName, @ClassId,  @Option ";

                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@TypeName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Select";
                sqlCon.Open();
                sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    cls.Add(new TblType()
                    {
                        TYPE_ID = Convert.ToInt32(sdr.GetValue(0)),
                        CLASS_ID = Convert.ToInt32(sdr.GetValue(2)),
                        TYPE_NAME = sdr.GetValue(1).ToString(),
                        CLASS_NAME = sdr.GetValue(3).ToString(),
                    });
                }
                sqlCon.Close();
                return Ok(cls);
            }
        }
        [HttpGet]
        [Route("api/Type/getTypeByClassId/{id?}")]
       public IHttpActionResult getTypeByClassId(int id)
        {
            List<TblType> cls = new List<TblType>();

            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteType @TypeId, @TypeName, @ClassId,  @Option ";

                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@TypeName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "GetByClassId";
                sqlCon.Open();
                sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    cls.Add(new TblType()
                    {
                        TYPE_ID = Convert.ToInt32(sdr.GetValue(0)),
                        CLASS_ID = Convert.ToInt32(sdr.GetValue(2)),
                        TYPE_NAME = sdr.GetValue(1).ToString()
                    });
                }
                sqlCon.Close();
                return Ok(cls);
            }
        }
        [HttpGet]
        [Route("api/Type/checkifexists/{id?}")]
        public IHttpActionResult checkifexists(int id)
        {
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteType @TypeId, @TypeName, @ClassId,  @Option ";

                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@TypeName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "IfExists";
                sqlCon.Open();
                var a = cmd.ExecuteScalar();

                sqlCon.Close();
                return Ok(a);
            }
        }
        public IHttpActionResult createType(TblType dbType)
        {
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteType @TypeId, @TypeName, @ClassId,  @Option ";

                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = dbType.CLASS_ID;
                cmd.Parameters.Add("@TypeName", SqlDbType.VarChar, 50).Value = dbType.TYPE_NAME;
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Create";

                cmd.ExecuteNonQuery();
                sqlCon.Close();
                return Ok();
            }
        }
        public IHttpActionResult getTypeById(int id)
        {
            TblType cls = new TblType();
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteType @TypeId, @TypeName, @ClassId,  @Option ";

                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value =0;
                cmd.Parameters.Add("@TypeName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "GetById";
                sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    cls.TYPE_ID = Convert.ToInt32(sdr.GetValue(0));
                    cls.CLASS_ID = Convert.ToInt32(sdr.GetValue(2));
                    cls.TYPE_NAME = sdr.GetValue(1).ToString();
                }
                sqlCon.Close();
                return Ok(cls);
            }
        }
        [HttpPut]
        public IHttpActionResult editClass(int id, TblType dbType)
        {
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteType @TypeId, @TypeName, @ClassId,  @Option ";

                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = dbType.CLASS_ID;
                cmd.Parameters.Add("@TypeName", SqlDbType.VarChar, 50).Value = dbType.TYPE_NAME;
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Edit";

                cmd.ExecuteNonQuery();
                sqlCon.Close();
                return Ok();
            }
        }
        [HttpDelete]
        public IHttpActionResult deleteType(int id)
        {
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteType @TypeId, @TypeName, @ClassId,  @Option ";

                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@TypeName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Delete";

                cmd.ExecuteNonQuery();
                sqlCon.Close();
                return Ok();
            }
        }
    }
}
