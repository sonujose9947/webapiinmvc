﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using WebApi.Models;
using System.Configuration;
using System.Web.UI.WebControls;

namespace WebApi.Controllers
{
    public class ClassController : ApiController
    {
        SqlConnection sqlCon = null;
        string con = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        SqlDataReader sdr;
        public IHttpActionResult getClass()
        {
            List<TblClass> cls= new List<TblClass>();
            using (sqlCon = new SqlConnection(con))
            {  
                SqlCommand sql_cmnd = new SqlCommand("SelectAllClass", sqlCon);
                sql_cmnd.CommandType = CommandType.StoredProcedure;
               sqlCon.Open();
               sdr=  sql_cmnd.ExecuteReader();

                while (sdr.Read())
            {
                cls.Add(new TblClass()
                {
                    CLASS_ID = Convert.ToInt32(sdr.GetValue(0)),
                    CLASS_NAME = sdr.GetValue(1).ToString()
                });
            }
                sqlCon.Close();
                return Ok(cls);
            }
        }
        [HttpGet]
        [Route("api/Class/checkifexists/{id?}")]
        public IHttpActionResult checkifexists(int id)
        {
            using (sqlCon = new SqlConnection(con))
            {
                    SqlCommand cmd = sqlCon.CreateCommand();
                cmd.CommandText = "Execute InsertUpdateDeleteClass @ClassId,@ClassName,@Option";

                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@ClassName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "IfExists";

                sqlCon.Open();
                var a = cmd.ExecuteScalar();

                sqlCon.Close();
                return Ok(a);
            }
        }
        public IHttpActionResult createClass(TblClass dbclass)
        {
                using (sqlCon = new SqlConnection(con))
                {
                    SqlCommand cmd = sqlCon.CreateCommand();
                    sqlCon.Open();
                    cmd.CommandText = "Execute InsertUpdateDeleteClass @ClassId,@ClassName,@Option";

                    cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                    cmd.Parameters.Add("@ClassName", SqlDbType.VarChar, 50).Value = dbclass.CLASS_NAME;
                    cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Create";

                    cmd.ExecuteNonQuery();
                    sqlCon.Close();
                    return Ok();
                }
        }

        public IHttpActionResult getClassById(int id)
        {
            TblClass cls= new TblClass();
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute InsertUpdateDeleteClass @ClassId,@ClassName,@Option";

                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@ClassName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "GetById";
                sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {

                    cls.CLASS_ID = Convert.ToInt32(sdr.GetValue(0));
                    cls.CLASS_NAME = sdr.GetValue(1).ToString();
                }
                sqlCon.Close();
                return Ok(cls);
            }
        }
        [HttpPut]
        public IHttpActionResult editClass(int id,TblClass dbclass)
        {
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute InsertUpdateDeleteClass @ClassId,@ClassName,@Option";

                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@ClassName", SqlDbType.VarChar, 50).Value = dbclass.CLASS_NAME;
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Edit";

                cmd.ExecuteNonQuery();
                sqlCon.Close();
                return Ok();
            }
        }
        [HttpDelete]
        public IHttpActionResult deleteClass(int id)
        {
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute InsertUpdateDeleteClass @ClassId,@ClassName,@Option";

                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@ClassName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Delete";

                cmd.ExecuteNonQuery();
                sqlCon.Close();
                return Ok();
            }
        }
    }
}
