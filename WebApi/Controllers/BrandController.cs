﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using WebApi.Models;
using System.Configuration;
using System.Web.UI.WebControls;

namespace WebApi.Controllers
{
    public class BrandController : ApiController
    {
        SqlConnection sqlCon = null;
        string con = ConfigurationManager.ConnectionStrings["myConnection"].ConnectionString;
        SqlDataReader sdr;
        public IHttpActionResult getBrand()
        {
            List<TblBrand> cls = new List<TblBrand>();

            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteBrand @BrandId, @BrandName, @ClassId, @TypeId, @Option";

                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@BrandName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Select";
                sqlCon.Open();
                sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    cls.Add(new TblBrand()
                    {
                        BRAND_ID = Convert.ToInt32(sdr.GetValue(0)),
                        BRAND_NAME = sdr.GetValue(1).ToString(),
                        CLASS_ID = Convert.ToInt32(sdr.GetValue(2)),
                        CLASS_NAME = sdr.GetValue(3).ToString(),
                        TYPE_ID = Convert.ToInt32(sdr.GetValue(4)),
                        TYPE_NAME = sdr.GetValue(5).ToString(),
                    });
                }
                sqlCon.Close();
                return Ok(cls);
            }
        }
        public IHttpActionResult createBrand(TblBrand dbBrand)
        {
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteBrand @BrandId, @BrandName, @ClassId, @TypeId, @Option";

                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = dbBrand.TYPE_ID;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = dbBrand.CLASS_ID;
                cmd.Parameters.Add("@BrandName", SqlDbType.VarChar, 50).Value = dbBrand.BRAND_NAME;
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Create";

                cmd.ExecuteNonQuery();
                sqlCon.Close();
                return Ok();
            }
        }
        public IHttpActionResult getBrandById(int id)
        {
            TblBrand cls = new TblBrand();
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteBrand @BrandId, @BrandName, @ClassId, @TypeId, @Option";

                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@BrandName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "GetById";
                sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    cls.BRAND_ID = Convert.ToInt32(sdr.GetValue(0));
                    cls.BRAND_NAME = sdr.GetValue(1).ToString();
                    cls.CLASS_ID = Convert.ToInt32(sdr.GetValue(2));
                    cls.TYPE_ID = Convert.ToInt32(sdr.GetValue(3));
                }
                sqlCon.Close();
                return Ok(cls);
            }
        }

        [HttpGet]
        [Route("api/Brand/getBrandByTypeId/{id?}")]
        public IHttpActionResult getBrandByTypeId(int id)
        {
            List<TblBrand> cls = new List<TblBrand>();

            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteBrand @BrandId, @BrandName, @ClassId, @TypeId, @Option";

                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@BrandName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "GetByTypeId";
                sqlCon.Open();
                sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    cls.Add(new TblBrand()
                    {
                        BRAND_ID= Convert.ToInt32(sdr.GetValue(0)),
                        BRAND_NAME = sdr.GetValue(1).ToString(),
                        CLASS_ID = Convert.ToInt32(sdr.GetValue(2)),
                        TYPE_ID = Convert.ToInt32(sdr.GetValue(3)),
                    });
                }
                sqlCon.Close();
                return Ok(cls);
            }
        }
        [HttpGet]
        [Route("api/Brand/checkifexists/{id?}")]
        public IHttpActionResult checkifexists(int id)
        {
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteBrand @BrandId, @BrandName, @ClassId, @TypeId, @Option";

                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@BrandName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "IfExists";
                sqlCon.Open();
                var a = cmd.ExecuteScalar();

                sqlCon.Close();
                return Ok(a);
            }
        }
        [HttpPut]
        public IHttpActionResult editBrand(int id, TblBrand dbBrand)
        {
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteBrand @BrandId, @BrandName, @ClassId, @TypeId, @Option";

                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = dbBrand.TYPE_ID;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = dbBrand.CLASS_ID;
                cmd.Parameters.Add("@BrandName", SqlDbType.VarChar, 50).Value = dbBrand.BRAND_NAME;
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Edit";

                cmd.ExecuteNonQuery();
                sqlCon.Close();
                return Ok();
            }
        }
        [HttpDelete]
        public IHttpActionResult deleteBrand(int id)
        {
            using (sqlCon = new SqlConnection(con))
            {
                SqlCommand cmd = sqlCon.CreateCommand();
                sqlCon.Open();
                cmd.CommandText = "Execute SelectInsertUpdateDeleteBrand @BrandId, @BrandName, @ClassId, @TypeId, @Option";

                cmd.Parameters.Add("@BrandId", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@TypeId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ClassId", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@BrandName", SqlDbType.VarChar, 50).Value = "";
                cmd.Parameters.Add("@Option", SqlDbType.VarChar, 50).Value = "Delete";

                cmd.ExecuteNonQuery();
                sqlCon.Close();
                return Ok();
            }
        }
    }
}
