﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class TblModel
    {
        public int MODEL_ID { get; set; }
        public string MODEL_NAME { get; set; }
        public int CLASS_ID { get; set; }
        public string CLASS_NAME { get; set; }
        public int TYPE_ID { get; set; }
        public string TYPE_NAME { get; set; }
        public int BRAND_ID { get; set; }
        public string BRAND_NAME { get; set; }

        [NotMapped]
        public List<TblClass> Class { get; set; }
        [NotMapped]
        public List<TblType> Type { get; set; }
        [NotMapped]
        public List<TblBrand> Brand { get; set; }
    }
}