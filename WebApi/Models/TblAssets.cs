﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class TblAssets
    {
        public int ASSET_ID { get; set; }
        public string ASSET_NAME { get; set; }
        public decimal ASSET_PRICE { get; set; }
        public DateTime INSTALLED_DATE { get; set; }

        public int CLASS_ID { get; set; }
        public string CLASS_NAME { get; set; }

        public int TYPE_ID { get; set; }
        public string TYPE_NAME { get; set; }

        public int BRAND_ID { get; set; }
        public string BRAND_NAME { get; set; }

        public int MODEL_ID { get; set; }
        public string MODEL_NAME { get; set; }

        public List<TblClass> Class { get; set; }
        public List<TblType> Type { get; set; }
        public List<TblBrand> Brand { get; set; }
        public List<TblModel> Model { get; set; }
    }
}