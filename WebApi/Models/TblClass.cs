﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class TblClass
    {
        public int CLASS_ID { get; set; }
        public string CLASS_NAME { get; set; }
    }
}