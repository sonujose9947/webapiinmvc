﻿using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class AssetsController : Controller
    {
        // GET: Assets
        public ActionResult Index()
        {
            IEnumerable<TblAssets> cls = null;
            HttpResponseMessage res = GlobalVariables.webApiClient.GetAsync("Assets").Result;
            cls = res.Content.ReadAsAsync<IEnumerable<TblAssets>>().Result;
            return View(cls);
        }
        public ActionResult CreateOrEdit(int id = 0)
        {
            IEnumerable<TblClass> cls = null;
            IEnumerable<TblType> typ = null;
            IEnumerable<TblBrand> brand = null;
            IEnumerable<TblModel> model = null;
            HttpResponseMessage res1 = GlobalVariables.webApiClient.GetAsync("Class").Result;
            cls = res1.Content.ReadAsAsync<IEnumerable<TblClass>>().Result;
            TblAssets assets = new TblAssets();
            if (id == 0)
            {
                HttpResponseMessage res2 = GlobalVariables.webApiClient.GetAsync("Type").Result;
                typ = res2.Content.ReadAsAsync<IEnumerable<TblType>>().Result;
                HttpResponseMessage res3 = GlobalVariables.webApiClient.GetAsync("Brand").Result;
                brand = res3.Content.ReadAsAsync<IEnumerable<TblBrand>>().Result;
                HttpResponseMessage res4 = GlobalVariables.webApiClient.GetAsync("Model").Result;
                model = res4.Content.ReadAsAsync<IEnumerable<TblModel>>().Result;
                assets.Class = cls.ToList<TblClass>();
                assets.Type = typ.ToList<TblType>();
                assets.Brand = brand.ToList<TblBrand>();
                assets.Model = model.ToList<TblModel>();
                assets.INSTALLED_DATE = DateTime.Now;
                ViewBag.Title = "Create";
                return View(assets);
            }
            else
            {
                HttpResponseMessage res5 = GlobalVariables.webApiClient.GetAsync("Assets/" + id.ToString()).Result;
                assets = res5.Content.ReadAsAsync<TblAssets>().Result;
                HttpResponseMessage res6 = GlobalVariables.webApiClient.GetAsync("Type/getTypeByClassId/" + assets.CLASS_ID).Result;
                typ = res6.Content.ReadAsAsync<IEnumerable<TblType>>().Result;
                HttpResponseMessage res7 = GlobalVariables.webApiClient.GetAsync("Brand/getBrandByTypeId/" + assets.TYPE_ID).Result;
                brand = res7.Content.ReadAsAsync<IEnumerable<TblBrand>>().Result;
                HttpResponseMessage res8 = GlobalVariables.webApiClient.GetAsync("Model/getModelByBrandId/" + assets.BRAND_ID).Result;
                model = res8.Content.ReadAsAsync<IEnumerable<TblModel>>().Result;
                assets.Class = cls.ToList<TblClass>();
                assets.Type = typ.ToList<TblType>();
                assets.Brand = brand.ToList<TblBrand>();
                assets.Model = model.ToList<TblModel>();
                ViewBag.Title = "Edit";
                return View(assets);
            }
        }

        [HttpPost]
        public ActionResult CreateOrEdit(TblAssets dbassets)
        {
            if (dbassets.ASSET_ID == 0)
            {
                HttpResponseMessage res = GlobalVariables.webApiClient.PostAsJsonAsync("Assets", dbassets).Result;
                TempData["SuccessMessage"] = "Saved Successfully";
            }
            else
            {
                HttpResponseMessage res = GlobalVariables.webApiClient.PutAsJsonAsync("Assets/" + dbassets.ASSET_ID, dbassets).Result;
                TempData["SuccessMessage"] = "Updated Successfully";

            }
            return RedirectToAction("Index");
        }
        public JsonResult FillType(int id)
        {
            IEnumerable<TblType> typ = null;
            HttpResponseMessage res1 = GlobalVariables.webApiClient.GetAsync("Type/getTypeByClassId/" + id).Result;
            typ = res1.Content.ReadAsAsync<IEnumerable<TblType>>().Result;
            return Json(typ, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FillBrand(int id)
        {
            IEnumerable<TblBrand> brand = null;
            HttpResponseMessage res1 = GlobalVariables.webApiClient.GetAsync("Brand/getBrandByTypeId/" + id).Result;
            brand = res1.Content.ReadAsAsync<IEnumerable<TblBrand>>().Result;
            return Json(brand, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FillModel(int id)
        {
            IEnumerable<TblModel> model = null;
            HttpResponseMessage res1 = GlobalVariables.webApiClient.GetAsync("Model/getModelByBrandId/" + id).Result;
            model = res1.Content.ReadAsAsync<IEnumerable<TblModel>>().Result;
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            if (id != 0)
            {
                HttpResponseMessage res = GlobalVariables.webApiClient.DeleteAsync("Assets/" + id.ToString()).Result;
                TempData["SuccessMessage"] = "Deleted Successfully";
            }
            return RedirectToAction("Index");
        }
    }
}