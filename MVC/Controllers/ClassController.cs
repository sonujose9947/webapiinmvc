﻿using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace MVC.Controllers
{
    public class ClassController : Controller
    {
        // GET: Class
        public ActionResult Index()
        {
            IEnumerable<TblClass> cls = null;
            HttpResponseMessage res = GlobalVariables.webApiClient.GetAsync("Class").Result;
            cls = res.Content.ReadAsAsync<IEnumerable<TblClass>>().Result;
            return View(cls);
        }

        public ActionResult CreateOrEdit(int id = 0)
        {
            if(id==0)
            { 
                return View(new TblClass());
            }
            else
            {
                HttpResponseMessage res = GlobalVariables.webApiClient.GetAsync("Class/"+ id.ToString()).Result;
                return View(res.Content.ReadAsAsync<TblClass>().Result);
            }

        }
        [HttpPost]
        public ActionResult CreateOrEdit(TblClass dbclass)
        {
            if (dbclass.CLASS_ID == 0)
            {
                HttpResponseMessage res = GlobalVariables.webApiClient.PostAsJsonAsync("Class", dbclass).Result;
                TempData["SuccessMessage"] = "Saved Successfully";
            }
            else
            {
                HttpResponseMessage res = GlobalVariables.webApiClient.PutAsJsonAsync("Class/" + dbclass.CLASS_ID, dbclass).Result;
                TempData["SuccessMessage"] = "Updated Successfully";

            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            if (id != 0)
            {
                HttpResponseMessage res1 = GlobalVariables.webApiClient.GetAsync("Class/checkifexists/" + id).Result;
                var a = res1.Content.ReadAsAsync<string>().Result;
                if (a != null)
                {
                    TempData["SuccessMessage"] = "Cannot Delete";
                }
                else
                {
                    HttpResponseMessage res = GlobalVariables.webApiClient.DeleteAsync("Class/" + id.ToString()).Result;
                    TempData["SuccessMessage"] = "Deleted Successfully";
                }
               
            }
            return RedirectToAction("Index");
        }
    }
}