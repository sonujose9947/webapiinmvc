﻿using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class ModelController : Controller
    {
        // GET: Model
        public ActionResult Index()
        {
            IEnumerable<TblModel> cls = null;
            HttpResponseMessage res = GlobalVariables.webApiClient.GetAsync("Model").Result;
            cls = res.Content.ReadAsAsync<IEnumerable<TblModel>>().Result;
            return View(cls);
        }
        public ActionResult CreateOrEdit(int id = 0)
        {
            IEnumerable<TblClass> cls = null;
            IEnumerable<TblType> typ = null;
            IEnumerable<TblBrand> brand = null;
            HttpResponseMessage res1 = GlobalVariables.webApiClient.GetAsync("Class").Result;
            cls = res1.Content.ReadAsAsync<IEnumerable<TblClass>>().Result;
            TblModel model = new TblModel();
            if (id == 0)
            {
                HttpResponseMessage res2 = GlobalVariables.webApiClient.GetAsync("Type").Result;
                typ = res2.Content.ReadAsAsync<IEnumerable<TblType>>().Result;
                HttpResponseMessage res3 = GlobalVariables.webApiClient.GetAsync("Brand").Result;
                brand = res3.Content.ReadAsAsync<IEnumerable<TblBrand>>().Result;
                model.Class = cls.ToList<TblClass>();
                model.Type = typ.ToList<TblType>();
                model.Brand = brand.ToList<TblBrand>();
                ViewBag.Title = "Create";
                return View(model);
            }
            else
            {
                HttpResponseMessage res4 = GlobalVariables.webApiClient.GetAsync("Model/" + id.ToString()).Result;
                model = res4.Content.ReadAsAsync<TblModel>().Result;
                HttpResponseMessage res5 = GlobalVariables.webApiClient.GetAsync("Type/getTypeByClassId/" + model.CLASS_ID).Result;
                typ = res5.Content.ReadAsAsync<IEnumerable<TblType>>().Result;
                HttpResponseMessage res6 = GlobalVariables.webApiClient.GetAsync("Brand/getBrandByTypeId/" + model.TYPE_ID).Result;
                brand = res6.Content.ReadAsAsync<IEnumerable<TblBrand>>().Result;
                model.Class = cls.ToList<TblClass>();
                model.Type = typ.ToList<TblType>();
                model.Brand = brand.ToList<TblBrand>();
                ViewBag.Title = "Edit";
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult CreateOrEdit(TblModel dbmodel)
        {
            if (dbmodel.MODEL_ID == 0)
            {
                HttpResponseMessage res = GlobalVariables.webApiClient.PostAsJsonAsync("Model", dbmodel).Result;
                TempData["SuccessMessage"] = "Saved Successfully";
            }
            else
            {
                HttpResponseMessage res = GlobalVariables.webApiClient.PutAsJsonAsync("Model/" + dbmodel.MODEL_ID, dbmodel).Result;
                TempData["SuccessMessage"] = "Updated Successfully";

            }
            return RedirectToAction("Index");
        }
        public JsonResult FillType(int id)
        {
             IEnumerable<TblType> typ = null;
            HttpResponseMessage res1 = GlobalVariables.webApiClient.GetAsync("Type/getTypeByClassId/" + id).Result;
            typ = res1.Content.ReadAsAsync<IEnumerable<TblType>>().Result;
            return Json(typ, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FillBrand(int id)
        {
            IEnumerable<TblBrand> brand = null;
            HttpResponseMessage res1 = GlobalVariables.webApiClient.GetAsync("Brand/getBrandByTypeId/" + id).Result;
            brand = res1.Content.ReadAsAsync<IEnumerable<TblBrand>>().Result;
            return Json(brand, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            if (id != 0)
            {
                HttpResponseMessage res1 = GlobalVariables.webApiClient.GetAsync("Model/checkifexists/" + id).Result;
                var a = res1.Content.ReadAsAsync<string>().Result;
                if (a != null)
                {
                    TempData["SuccessMessage"] = "Cannot Delete";
                }
                else
                {
                    HttpResponseMessage res = GlobalVariables.webApiClient.DeleteAsync("Model/" + id.ToString()).Result;
                    TempData["SuccessMessage"] = "Deleted Successfully";
                }
            }
            return RedirectToAction("Index");
        }
    }
}