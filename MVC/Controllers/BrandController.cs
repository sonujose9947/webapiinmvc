﻿using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class BrandController : Controller
    {
        // GET: Brand
        public ActionResult Index()
        {
            IEnumerable<TblBrand> cls = null;
            HttpResponseMessage res = GlobalVariables.webApiClient.GetAsync("Brand").Result;
            cls = res.Content.ReadAsAsync<IEnumerable<TblBrand>>().Result;
            return View(cls);
        }
        public ActionResult CreateOrEdit(int id = 0)
        {
            IEnumerable<TblClass> cls = null;
            IEnumerable<TblType> typ = null;
            HttpResponseMessage res1 = GlobalVariables.webApiClient.GetAsync("Class").Result;
            cls = res1.Content.ReadAsAsync<IEnumerable<TblClass>>().Result;
            TblBrand brand = new TblBrand();
            if (id == 0)
            {
                HttpResponseMessage res2 = GlobalVariables.webApiClient.GetAsync("Type").Result;
                typ = res2.Content.ReadAsAsync<IEnumerable<TblType>>().Result;
                brand.Class = cls.ToList<TblClass>();
                brand.Type = typ.ToList<TblType>();
                ViewBag.Title = "Create";
                return View(brand);
            }
            else
            {
                HttpResponseMessage res3 = GlobalVariables.webApiClient.GetAsync("Brand/" + id.ToString()).Result;
                brand = res3.Content.ReadAsAsync<TblBrand>().Result;
                HttpResponseMessage res4 = GlobalVariables.webApiClient.GetAsync("Type/getTypeByClassId/" + brand.CLASS_ID).Result;
                typ = res4.Content.ReadAsAsync<IEnumerable<TblType>>().Result;
                brand.Class = cls.ToList<TblClass>();
                brand.Type = typ.ToList<TblType>();
                ViewBag.Title = "Edit";
                return View(brand);
            }
        }

        [HttpPost]
        public ActionResult CreateOrEdit(TblBrand dbbrand)
        {
            if (dbbrand.BRAND_ID == 0)
            {
                HttpResponseMessage res = GlobalVariables.webApiClient.PostAsJsonAsync("Brand", dbbrand).Result;
                TempData["SuccessMessage"] = "Saved Successfully";
            }
            else
            {
                HttpResponseMessage res = GlobalVariables.webApiClient.PutAsJsonAsync("Brand/" + dbbrand.BRAND_ID, dbbrand).Result;
                TempData["SuccessMessage"] = "Updated Successfully";

            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            if (id != 0)
            {
                HttpResponseMessage res1 = GlobalVariables.webApiClient.GetAsync("Brand/checkifexists/" + id).Result;
                var a = res1.Content.ReadAsAsync<string>().Result;
                if (a != null)
                {
                    TempData["SuccessMessage"] = "Cannot Delete";
                }
                else
                {
                    HttpResponseMessage res = GlobalVariables.webApiClient.DeleteAsync("Brand/" + id.ToString()).Result;
                    TempData["SuccessMessage"] = "Deleted Successfully";
                }
            }
            return RedirectToAction("Index");
        }
        public JsonResult FillType(int id)
        {
           IEnumerable<TblType> typ = null;
            HttpResponseMessage res1 = GlobalVariables.webApiClient.GetAsync("Type/getTypeByClassId/" + id).Result;
            typ = res1.Content.ReadAsAsync<IEnumerable<TblType>>().Result;
            return Json(typ, JsonRequestBehavior.AllowGet);
        }

    }
}