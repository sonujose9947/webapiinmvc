﻿using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class TypeController : Controller
    {
        // GET: Type
        public ActionResult Index()
        {
            IEnumerable<TblType> cls = null;
            HttpResponseMessage res = GlobalVariables.webApiClient.GetAsync("Type").Result;
            cls = res.Content.ReadAsAsync<IEnumerable<TblType>>().Result;
            return View(cls);
        }
        public ActionResult CreateOrEdit(int id = 0)
        {
            IEnumerable<TblClass> cls = null;
            HttpResponseMessage res1 = GlobalVariables.webApiClient.GetAsync("Class").Result;
            cls = res1.Content.ReadAsAsync<IEnumerable<TblClass>>().Result;
            TblType type = new TblType();
              if (id == 0)
                {
                type.Class = cls.ToList<TblClass>();
               return View(type);
            }
            else
            {
                HttpResponseMessage res2 = GlobalVariables.webApiClient.GetAsync("Type/" + id.ToString()).Result;
                type = res2.Content.ReadAsAsync<TblType>().Result;
                type.Class = cls.ToList<TblClass>();
                return View(type);
            }

        }
        [HttpPost]
        public ActionResult CreateOrEdit(TblType dbtype)
        {
            if (dbtype.TYPE_ID == 0)
            {
                HttpResponseMessage res = GlobalVariables.webApiClient.PostAsJsonAsync("Type", dbtype).Result;
                TempData["SuccessMessage"] = "Saved Successfully";
            }
            else
            {
                HttpResponseMessage res = GlobalVariables.webApiClient.PutAsJsonAsync("Type/" + dbtype.TYPE_ID, dbtype).Result;
                TempData["SuccessMessage"] = "Updated Successfully";

            }
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {
            if (id != 0)
            {
                HttpResponseMessage res1 = GlobalVariables.webApiClient.GetAsync("Type/checkifexists/" + id).Result;
                var a = res1.Content.ReadAsAsync<string>().Result;
                if (a != null)
                {
                    TempData["SuccessMessage"] = "Cannot Delete";
                }
                else
                {
                    HttpResponseMessage res = GlobalVariables.webApiClient.DeleteAsync("Type/" + id.ToString()).Result;
                    TempData["SuccessMessage"] = "Deleted Successfully";
                }
            }
            return RedirectToAction("Index");
        }
    }
}