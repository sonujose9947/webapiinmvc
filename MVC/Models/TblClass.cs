﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class TblClass
    {
        public int CLASS_ID { get; set; }
        [Display(Name = "Class Name")]
        [Required(ErrorMessage = "This field is required")]
        public string CLASS_NAME { get; set; }
    }
}