﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class TblType
    {
        public int TYPE_ID { get; set; }
        [Display(Name = "Type Name")]
        [Required(ErrorMessage = "This field is required")]
        public string TYPE_NAME { get; set; }
        [Display(Name = "Class")]
        [Required(ErrorMessage = "This field is required")]
        public int CLASS_ID { get; set; }
        public string CLASS_NAME { get; set; }

        [NotMapped]
        public List<TblClass> Class { get; set; }
    }
}