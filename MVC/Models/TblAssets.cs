﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class TblAssets
    {
        public int ASSET_ID { get; set; }

        [Display(Name = "Asset Name")]
        [Required(ErrorMessage = "This field is required")]
        public string ASSET_NAME { get; set; }

        [Display(Name = "Asset Price")]
        [RegularExpression(@"^\d+.\d{0,2}$")]
        [Required(ErrorMessage = "This field is required")]
        public decimal ASSET_PRICE { get; set; }

        [Display(Name = "Installed Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "This field is required")]
        public DateTime INSTALLED_DATE { get; set; }

        [Display(Name = "Class")]
        [Required(ErrorMessage = "This field is required")]
        public int CLASS_ID { get; set; }
        public string CLASS_NAME { get; set; }

        [Display(Name = "Type")]
        [Required(ErrorMessage = "This field is required")]
        public int TYPE_ID { get; set; }
        public string TYPE_NAME { get; set; }

        [Display(Name = "Brand")]
        [Required(ErrorMessage = "This field is required")]
        public int BRAND_ID { get; set; }
        public string BRAND_NAME { get; set; }

        [Display(Name = "Model")]
        [Required(ErrorMessage = "This field is required")]
        public int MODEL_ID { get; set; }
        public string MODEL_NAME { get; set; }
        [NotMapped]
        public List<TblClass> Class { get; set; }
        [NotMapped]
        public List<TblType> Type { get; set; }
        [NotMapped]
        public List<TblBrand> Brand { get; set; }
        [NotMapped]
        public List<TblModel> Model { get; set; }
    }
}